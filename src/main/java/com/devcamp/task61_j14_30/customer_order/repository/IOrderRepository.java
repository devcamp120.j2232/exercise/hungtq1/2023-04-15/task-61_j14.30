package com.devcamp.task61_j14_30.customer_order.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task61_j14_30.customer_order.model.Order;

public interface IOrderRepository extends JpaRepository<Order, Long> {

    Order findByOrderId(long orderId);

}
