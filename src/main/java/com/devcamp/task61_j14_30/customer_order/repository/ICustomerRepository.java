package com.devcamp.task61_j14_30.customer_order.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task61_j14_30.customer_order.model.Customer;

public interface ICustomerRepository extends JpaRepository<Customer, Long> {
  Customer findByCustomerId(Long customerId);
}
