package com.devcamp.task61_j14_30.customer_order.service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task61_j14_30.customer_order.model.Order;
import com.devcamp.task61_j14_30.customer_order.model.Product;
import com.devcamp.task61_j14_30.customer_order.repository.IOrderRepository;

@Service
public class OrderService {
  @Autowired
  IOrderRepository pIOrderRepository;

  public ArrayList<Order> getAllOrders() {
    ArrayList<Order> pOrders = new ArrayList<>();
    pIOrderRepository.findAll().forEach(pOrders::add);
    return pOrders;
  }

  public Set<Product> getProductsByOrderId(long orderId){
    Order vOrder = pIOrderRepository.findByOrderId(orderId);
    if ( vOrder != null){
        return vOrder.getProducts();
    }
    else return null;
}
}
