package com.devcamp.task61_j14_30.customer_order.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task61_j14_30.customer_order.model.Product;
import com.devcamp.task61_j14_30.customer_order.repository.IProductRepository;

@Service
public class ProductService {
    @Autowired
    IProductRepository productRepository;
    public List<Product> getAllProducts(){
        List<Product> orders = new ArrayList<Product>();
        productRepository.findAll().forEach(orders::add);
        return orders;
    }
}
