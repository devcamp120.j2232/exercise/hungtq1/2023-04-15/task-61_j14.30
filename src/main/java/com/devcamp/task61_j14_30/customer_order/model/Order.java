package com.devcamp.task61_j14_30.customer_order.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "orders")
public class Order {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long orderId;
  @Column(name = "order_code")
  private String orderCode;
  @Column(name = "pizza_size")
  private String pizzaSize;
  @Column(name = "pizza_type")
  private String pizzaType;
  @Column(name = "voucher_code")
  private String voucherCode;
  @Column(name = "price")
  private Long price;
  @Column(name = "paid")
  private Long paid;

  @ManyToOne
  @JsonBackReference
  @JoinColumn(name = "customerId")
  private Customer customer;

  @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
  @JsonManagedReference
  private Set<Product> products;

  public Order() {
  }

  

  public Order(Long order_id, String orderCode, String pizzaSize, String pizzaType, String voucherCode, Long price,
      Long paid, Customer customer, Set<Product> products) {
    this.orderId = order_id;
    this.orderCode = orderCode;
    this.pizzaSize = pizzaSize;
    this.pizzaType = pizzaType;
    this.voucherCode = voucherCode;
    this.price = price;
    this.paid = paid;
    this.customer = customer;
    this.products = products;
  }



  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long order_id) {
    this.orderId = order_id;
  }

  public String getOrderCode() {
    return orderCode;
  }

  public void setOrderCode(String orderCode) {
    this.orderCode = orderCode;
  }

  public String getPizzaSize() {
    return pizzaSize;
  }

  public void setPizzaSize(String pizzaSize) {
    this.pizzaSize = pizzaSize;
  }

  public String getPizzaType() {
    return pizzaType;
  }

  public void setPizzaType(String pizzaType) {
    this.pizzaType = pizzaType;
  }

  public String getVoucherCode() {
    return voucherCode;
  }

  public void setVoucherCode(String voucherCode) {
    this.voucherCode = voucherCode;
  }

  public Long getPrice() {
    return price;
  }

  public void setPrice(Long price) {
    this.price = price;
  }

  public Long getPaid() {
    return paid;
  }

  public void setPaid(Long paid) {
    this.paid = paid;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public Set<Product> getProducts() {
    return products;
  }

  public void setProducts(Set<Product> products) {
    this.products = products;
  }


}
